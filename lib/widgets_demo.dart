import 'package:flutter/material.dart';

class MyWidgetsDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyWidgets(),
    );
  }
}

class MyWidgets extends StatefulWidget {
  @override
  _MyWidgetsState createState() => _MyWidgetsState();
}

class _MyWidgetsState extends State<MyWidgets> {

  var inputStr = "";
  var isEnabled = false;
  final txtController = TextEditingController();
  var isExpanded = false;
  var checked = false;
  var radioValue = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Input Widgets"),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(hintText: "Write Something here"),
              onChanged: (var textinput){
                  setState(() {
//                    inputStr = textinput;
                      inputStr = txtController.text.length.toString();
                  });
              },
              controller: txtController,
            ),
            SizedBox(height: 20.0, width: double.infinity,),
            Text(inputStr),
            SizedBox(height: 20.0, width: double.infinity,),
            Switch(
              activeColor: Colors.green,
              activeTrackColor: Colors.greenAccent,
              value: isEnabled,
              onChanged: (bool val){
                setState(() {
                  isEnabled = val;
                  print(isEnabled);
                });
              },
            ),
            SizedBox(height: 20.0, width: double.infinity,),
            ExpansionPanelList(
              expansionCallback: (i, bool val){
                setState(() {
                  isExpanded = !val;
                });
              },
              children: [
                ExpansionPanel(
                    body: Container(
                      padding: EdgeInsets.all(20.0),
                      child: Text('Hello'),
                    ),
                    headerBuilder: (BuildContext context, bool val){
                      return Center(
                        child: Text('Header (tap on me)', style: TextStyle(fontSize:  18.0),),
                      );
                    },
                  isExpanded: isExpanded
                )
              ],
            ),
            SizedBox(height: 20.0, width: double.infinity,),
            Checkbox(
              onChanged: (bool val){
                setState(() {
                  checked = val;
                });
              },
              value: checked,
            ),
            SizedBox(height: 20.0, width: double.infinity,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Radio(
                  onChanged: (String val){
                    setRadioValue(val);
                  },
                  value: 'First',
                  activeColor: Colors.green,
                  groupValue: radioValue,
                ),
                Radio(
                  onChanged: (String val){
                    setRadioValue(val);
                  },
                  value: 'Second',
                  activeColor: Colors.green,
                  groupValue: radioValue,
                ),
                Radio(
                  onChanged: (String val){
                    setRadioValue(val);
                  },
                  value: 'Third',
                  activeColor: Colors.green,
                  groupValue: radioValue,
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  setRadioValue(String value)
  {
    setState(() {
      radioValue = value;
      print('Radio onChange: $radioValue');
    });
  }
}

