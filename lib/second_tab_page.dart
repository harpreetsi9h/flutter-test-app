import 'package:flutter/material.dart';

class SecondTabPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text('Second Tab Page', style: TextStyle(
            color: Colors.blueAccent,
            fontSize: 25.0,
            fontFamily: 'Staatliches'
        ),),
      ),
    );
  }
}