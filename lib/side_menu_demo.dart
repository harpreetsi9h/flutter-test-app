import 'package:flutter/material.dart';
import 'about_page.dart';

class SideMenuDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MySideDrawer(),
      theme: ThemeData(
        primaryColor: Colors.deepPurple
      ),
    );
  }
}

class MySideDrawer extends StatefulWidget {
  @override
  _MySideDrawerState createState() => _MySideDrawerState();
}

class _MySideDrawerState extends State<MySideDrawer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Side Menu"),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text('Happy'),
              accountEmail: Text('happysi9h@gmail.com'),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage('http://i.pravatar.cc/300'),
              ),
            ),
            ListTile(
              title: Text("About Page"),
              onTap: (){
                Navigator.of(context).pop();
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AboutPage()));
              },
            ),
            Divider(
              color: Colors.blueGrey,
              height: 4.0,
            ),ListTile(
              title: Text("About Page"),
              onTap: (){
                Navigator.of(context).pop();
                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AboutPage()));
              },
            )
          ],
        ),
      ),
    );
  }
}

