import 'package:flutter/material.dart';
import 'first_tab_page.dart' as firstpage;
import 'second_tab_page.dart' as secondpage;

class TabsDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.teal
      ),
      home: MyTabs(),
    );
  }
}


class MyTabs extends StatefulWidget {
  @override
  _MyTabsState createState() => _MyTabsState();
}

class _MyTabsState extends State<MyTabs> with SingleTickerProviderStateMixin {

  TabController tabController;


  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }


  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tabs Demo', style: TextStyle(fontFamily: 'Staatliches'),),
        backgroundColor: Colors.teal,
        centerTitle: true,
        bottom: TabBar(
          controller: tabController,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.account_balance),),
            Tab(icon: Icon(Icons.account_circle),)
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
          children: <Widget> [
        firstpage.FirstTabPage(),
        secondpage.SecondTabPage()
      ]),
    );
  }
}
