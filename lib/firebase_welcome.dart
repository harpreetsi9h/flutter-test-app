import 'package:flutter/material.dart';

class FirebaseWelcome extends StatefulWidget
{
  @override
  State<StatefulWidget> createState() => WelcomeScreen();
}

class WelcomeScreen extends State<FirebaseWelcome>
{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigoAccent,
        body: Container(
          child: Center(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.accessibility, size: 60.0, color: Colors.black87,),
                  Text('WELCOME',
                      style: new TextStyle(
                          color: Colors.black12,
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold
                      )
                  ),
                  Container(
                      padding: const EdgeInsets.all(30.0),
                      child: FlatButton.icon(
                          onPressed:() =>  Navigator.of(context).pop(true),
                          icon: Icon(
                            Icons.exit_to_app,
                            color: Colors.black12,
                          ),
                          label: Text('Logout',
                            style: TextStyle(color: Colors.lightBlueAccent),
                          )
                      )
                  ),
                ]
            ),
          ),
        )
    );
  }
}