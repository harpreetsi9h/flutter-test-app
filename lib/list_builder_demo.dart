import 'package:flutter/material.dart';

class ListDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Builder', style: TextStyle(fontFamily: 'Staatliches')),
        centerTitle: true,
      ),
      body: Container(
        child: _MyList()
      )
    );
  }
}

class _MyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: EdgeInsets.all(5.0),
        itemBuilder: (context, i) {
          return ListTile(
            title: Text('Some Username',
            style: TextStyle(color: Colors.black87, fontFamily: 'Charm'),),
            subtitle: Text('Online', style: TextStyle(color: Colors.green, fontFamily: 'Poppins')),
            leading: Icon(Icons.face, color: Colors.blue,),
            trailing: RaisedButton(
              child: Text('Remove', style: TextStyle(fontFamily: 'Roboto'),),
              onPressed: (){
                deleteDialog(context).then((value){
                  print('delete pressed $value');
                });
              },
            ),
          );
        }
    );
  }
}

Future<bool> deleteDialog(BuildContext context){
  return showDialog(context: context,
    builder: (BuildContext context){
      return AlertDialog(
          title: Text('Are you sure to delete?', style: TextStyle(fontFamily: 'Poppins'),),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes', style: TextStyle(fontFamily: 'Roboto')),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
            FlatButton(
              child: Text('No', style: TextStyle(fontFamily: 'Roboto')),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            )
          ],
      );
    }
  );
}

