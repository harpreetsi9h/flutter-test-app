import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class HttpDemo extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HttpListViewDemo(),
      theme: ThemeData(
        primaryColor: Colors.blueGrey
      ),
    );
  }
}

class HttpListViewDemo extends StatefulWidget {
  @override
  _HttpListViewDemoState createState() => _HttpListViewDemoState();
}

class _HttpListViewDemoState extends State<HttpListViewDemo> {

  String url = 'https://randomuser.me/api/?results=20';

  List data;

  Future<String> makeRequest() async
  {
    var response = await http.get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

    setState(() {
      var resultData = JsonCodec().decode(response.body);
      data = resultData["results"];
    });

  }


  @override
  void initState() {
    super.initState();
    makeRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contact List"),
      ),
      body: ListView.builder(itemCount: data==null ? 0 : data.length,
        itemBuilder: (BuildContext context, i){
          return ListTile(
            title: Text(data[i]["name"]["first"]),
            subtitle: Text(data[i]["phone"]),
            leading: CircleAvatar(
              backgroundImage: NetworkImage(data[i]["picture"]["thumbnail"]),
            ),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ContactDetailsPage(data[i])));
            },
          );
        }),
    );
  }
}


class ContactDetailsPage extends StatelessWidget {

  ContactDetailsPage(this.data);
  final data;

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text('Contact Details'),
      centerTitle: true,
    ),
    body: Center(
      child: Container(
        width: 150.0,
        height: 150.0,
        decoration: BoxDecoration(
          color: Color(0xff7c94b6),
          image: DecorationImage(image: NetworkImage(data["picture"]["large"]),
          fit: BoxFit.cover),
          borderRadius: BorderRadius.all(Radius.circular(75.0)),
          border: Border.all(
            color: Colors.red,
            width: 4.0
          )
        ),
      ),
    ),
  );

}



