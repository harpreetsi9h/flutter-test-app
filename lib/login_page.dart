import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';

class LoginPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

final ThemeData DefaultTheme = ThemeData(
  primarySwatch: Colors.blue,
  accentColor: Colors.black,
);


enum FormType{
  login,
  register
}

class _LoginPageState extends State<LoginPage>
{

  String _email;
  String _password;
  FormType _formType = FormType.login;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  final formKey = GlobalKey<FormState>();

  void hideKb()
  {
      SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  void moveToRegister()
  {
    formKey.currentState.reset();
    print('Register Clicked: $_formType');
    setState(() {
      _formType = FormType.register;
    });
  }

  void moveToLogin()
  {
    formKey.currentState.reset();
    print('Login Clicked: $_formType');
    setState((){
      _formType = FormType.login;
    });
  }

  bool validateAndSave()
  {
    final form = formKey.currentState;
    form.save();
    if(form.validate())
    {
      return true;
    }

    return false;
  }

  void firebaseSubmit(BuildContext context) async
  {
      hideKb();
      if(validateAndSave())
      {
        try {

          if(_formType==FormType.login) {
            FirebaseUser user = await _auth.signInWithEmailAndPassword(
                email: _email, password: _password);
            print('Signed in: ${user.uid}');

            Navigator.of(context).pushNamed('/firebase_welcome');
//            Scaffold.of(context).showSnackBar(new SnackBar(
//              content: new Text("Signed: ${user.uid}"),
//              duration: const Duration(seconds: 5),
//            ));
          }
          else
          {
              FirebaseUser user = await _auth.createUserWithEmailAndPassword(email: _email, password: _password);
              print('Registered User: ${user.uid}');
              Navigator.of(context).pushNamed('/firebase_welcome');
          }

        }
        catch(e)
        {
          print('Error: $e');
          Scaffold.of(context).showSnackBar(new SnackBar(
            content: Text("Error: $e"),
            duration: const Duration(seconds: 5),
          ));
        }
      }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Builder(builder: (BuildContext context) {
        return Container(
            child: Form(
                key: formKey,
                child: Container(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: buildIcons() + buildInputs() + buildSubmitButtons()
                  ),
                )
            )
        );
      })
    );
  }

  List<Widget> buildIcons()
  {
      return[
         Container(
          child: Center(
            child: Icon(Icons.developer_mode,
              size: 80.0,),
          ),
          padding: const EdgeInsets.all(20.0),
        )
      ];
  }

  List<Widget> buildInputs(){
      return [
          TextFormField(
            decoration: InputDecoration(
                labelText: 'Email',
                labelStyle: TextStyle(color: Colors.tealAccent)
            ),
            validator: (value) => value.isEmpty ? "Email can't be Empty" : null,
            onSaved: (value) => _email = value,
            keyboardType: TextInputType.emailAddress,
          ),
          TextFormField(
            decoration: InputDecoration(
              labelText: 'Password',
              labelStyle: TextStyle(color: Colors.tealAccent),
            ),
            validator: (value) => value.isEmpty ? "Password can't be Empty" : null,
            obscureText: true,
            onSaved: (value) => _password = value,
          ),
      ];
  }

  List<Widget> buildSubmitButtons()
  {
      if(_formType==FormType.login) {

        return [
          Container(
            padding: const EdgeInsets.only(top: 20.0),
            child: MaterialButton(
              onPressed: () => firebaseSubmit(context),
              child: Text('LOGIN', style: TextStyle(fontSize: 20.0),),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 20.0),
            child: MaterialButton(
              onPressed: moveToRegister,
              child: Text('Create an Account', style: TextStyle(
                  fontSize: 16.0, color: Colors.tealAccent),),
            ),
          )
        ];

      }
      else {

        return [
          Container(
            padding: const EdgeInsets.only(top: 20.0),
            child: MaterialButton(
              onPressed: () => firebaseSubmit(context),
              child: Text('CREATE AN ACCOUNT', style: TextStyle(fontSize: 20.0),),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 20.0),
            child: MaterialButton(
              onPressed: moveToLogin,
              child: Text('Have an Account? Login', style: TextStyle(
                  fontSize: 16.0, color: Colors.lightBlueAccent[100]),),
            ),
          )
        ];

      }
  }

}

