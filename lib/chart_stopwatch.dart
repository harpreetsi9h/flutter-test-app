import 'package:flutter/material.dart';

class ChartDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyStopwatchChart(),
    );
  }
}


class MyStopwatchChart extends StatefulWidget {
  @override
  _MyStopwatchChartState createState() => _MyStopwatchChartState();
}

class _MyStopwatchChartState extends State<MyStopwatchChart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stopwatch'),
        centerTitle: true,
      ),
      body: Container(
       padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FloatingActionButton(
                  backgroundColor: Colors.green,
                  onPressed: (){},
                  child: Icon(Icons.play_arrow),
                ),
                SizedBox(width: 20.0,),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.stop),
                )
              ],
            ),
          ],
        )
      ),
    );
  }
}
