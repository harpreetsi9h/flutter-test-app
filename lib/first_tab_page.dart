import 'package:flutter/material.dart';

class FirstTabPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text('First Tab Page', style: TextStyle(
          color: Colors.purple,
          fontSize: 25.0,
            fontFamily: 'Staatliches'
        ),),
      ),
    );
  }
}
