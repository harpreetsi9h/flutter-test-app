import 'package:flutter/material.dart';

class SliversDemo extends StatefulWidget {
  @override
  _SliversDemoState createState() => _SliversDemoState();
}

class _SliversDemoState extends State<SliversDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text("Sliver Effect"),
            floating: false,
            expandedHeight: 250.0,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.network('https://placeimg.com/480/320/any', fit: BoxFit.cover ,),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index)=>
            Card(
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.transparent,
                      backgroundImage: NetworkImage('http://i.pravatar.cc/300'),
                    ),
                    SizedBox(width: 10.0,),
                    Text('I am Card Content!!')
                  ],
                ),
              ),
            )
            ),
          )
        ],
      ),
    );
  }
}
