import 'package:flutter/material.dart';
import 'login_page.dart';
import 'layout_demo.dart';
import 'firebase_welcome.dart';
import 'list_builder_demo.dart';
import 'tabs_demo.dart';
import 'http_demo.dart';
import 'widgets_demo.dart';
import 'side_menu_demo.dart';
import 'slivers_demo.dart';
import 'chart_stopwatch.dart';

void main() => runApp(new MyApp());

final ThemeData defaultTheme = new ThemeData(
  primarySwatch: Colors.blue,
  accentColor: Colors.lightBlue,
  fontFamily: 'Roboto'
);

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: defaultTheme,
      home: MainScreen(),
      routes: <String, WidgetBuilder> {
        '/layout_demo' : (BuildContext context) => BuildingLayoutDemo(),
        '/login_page' : (BuildContext context) => LoginPage(),
        '/firebase_welcome' : (BuildContext context) => FirebaseWelcome(),
        '/list_builder_demo' : (BuildContext context) => ListDemo(),
        '/tabs_demo' : (BuildContext context) => TabsDemo(),
        '/http_demo' : (BuildContext context) => HttpDemo(),
        '/widgets_demo' : (BuildContext context) => MyWidgetsDemo(),
        '/side_menu_demo' : (BuildContext context) => SideMenuDemo(),
        '/sliver_demo' : (BuildContext context) => SliversDemo(),
        '/chart_stopwatch' : (BuildContext context) => ChartDemo()
      },
    );
  }
}


class MainScreen extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Demos'),
      ),
      body: Container(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            MyButton('Building Layout', '/layout_demo'),
            MyButton('Firebase login', '/login_page'),
            MyButton('List Builder Demo', '/list_builder_demo'),
            MyButton('Tabs Demo', '/tabs_demo'),
            MyButton('Http & Json Handling', '/http_demo'),
            MyButton('Input Widgets', '/widgets_demo'),
            MyButton('Side Menu', '/side_menu_demo'),
            MyButton('Sliver', '/sliver_demo'),
            MyButton('Stopwatch', '/chart_stopwatch')
          ],
        ),
      ),
    );
  }
}

void navigate(BuildContext context ,String screen)
{
    Navigator.of(context).pushNamed(screen);
}

class MyButton extends StatelessWidget
{

  String title, screen;

  MyButton(String title, String screen)
  {
      this.title = title;
      this.screen = screen;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 15.0),
      child: MaterialButton(
        color: DefaultTheme.primaryColor,
        textColor: Colors.white,
        onPressed:() => navigate(context,screen),
        child: Text(title),
      )
    );
  }
}

